This project uses Java/ Serenity BDD framework with Cucumeber and Gradle as a build tool. 

Once the project is cloned you should be able to run 

1. Gradle clean build
2. Gradle test aggreagte

There are a couple of Api tests testing the following api from any-api.com - https://api.deutschebahn.com/freeplan/v1

These should be run and after in the target\site\serenity path you can access the index.html to see a test report.

The code is also stored on github, this is from where the prject was imported from.

https://github.com/Vbec05/QaTechnicalTest

The project structure is as follows
    1 Common code
        - Properties files for Data and Environment vaiable
        - Before test class
    2 Test
        - Class with the code to run the tests



I did try to set up a CI/CD pipeline in GitLab but I could not get it to run even though it works locally.

Below is the error and i was unsure if it was the image I was using. This was my first time working with GitLab.

ava.lang.ExceptionInInitializerError
        Caused by: java.util.MissingResourceException
com.leaseplan.tests.Arrivals > allArrivalsBerlinInvalidDate FAILED
    java.lang.NoClassDefFoundError



